from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoListItemForm
# Create your views here.


def todo_list_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list_list": todos,
    }
    return render(request, "todos/list.html", context)


def todo_list_detail(request, id=id):
    todos = get_object_or_404(TodoList, id=id)
    context = {
        "todo_list_detail": todos,
    }
    return render(request, 'todos/details.html', context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            list = form.save()
            return redirect("todo_list_detail", id=list.id)
    else:
        form = TodoListForm()
    context = {"form": form}
    return render(request, "todos/create.html", context)


def todo_list_update(request, id=id):
    form = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=form)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        holder = TodoListForm(instance=form)
    context = {
        "todo_list_update": form,
        "post_form": holder,
    }
    return render(request, "todos/edit.html", context)


def todo_list_delete(request, id):
    todo_list_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        todo_list_instance.delete()
        return redirect("todo_list_list")
    return render(request, "todos/delete.html")


def todo_item_create(request):
    if request.method == "POST":
        itemform = TodoListItemForm(request.POST)
        if itemform.is_valid():
            cat = itemform.save()
            return redirect("todo_list_detail", id=cat.list.id)
    else:
        itemform = TodoListItemForm()
    context = {
        "itemform": itemform,
    }
    return render(request, "todos/create_item.html", context)


def todo_item_update(request, id=id):
    form = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoListItemForm(request.POST, instance=form)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id=id)
    else:
        form = TodoListItemForm(instance=form)
    context = {
        "post_form": form,
    }
    return render(request, "todos/edit_item.html", context)
